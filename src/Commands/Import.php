<?php

namespace App\Commands;

use App\Commands\Provider\Gitlab;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpKernel\KernelInterface;

#[AsCommand(
    name: 'import',
    description: 'Run the importer'
)]
class Import extends Command
{

    public function __construct(
        string                            $name = null,
        private readonly ?KernelInterface $appKernel = null,
        private readonly ?Gitlab           $gitlabProvider = null,
    )
    {
        parent::__construct($name);
    }

    /**
     * Execute the command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int 0 if everything went fine, or an exit code.
     * @throws \JsonException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $result = $this->gitlabProvider->getPackages($io, [
            'token' => $input->getOption('gitlab-token'),
            'server' => $input->getOption('gitlab-server'),
            'limit' => $input->getOption('gitlab-limit'),
        ]);

        $packages = [];
        array_walk($result, static function($package) use (&$packages) {
            $packages += array_keys($package['dependencies']);
        });
        $packages = array_unique($packages);
        sort($packages);

        if (empty($result)) {
            $io->error('No composer data could be loaded. Abort!');
            return Command::FAILURE;
        }
        file_put_contents($this->appKernel->getProjectDir() . '/var/data.json', json_encode([
            'data' => $result,
            'packages' => $packages
        ], JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT));
        return Command::SUCCESS;
    }

    protected function configure()
    {
        $this->addOption('gitlab-token', '', InputOption::VALUE_OPTIONAL, '');
        $this->addOption('gitlab-server', '', InputOption::VALUE_OPTIONAL, '');
        $this->addOption('gitlab-limit', '', InputOption::VALUE_OPTIONAL, '');
    }


}