<?php

namespace App\Commands\Provider;

use Symfony\Component\Console\Style\SymfonyStyle;

interface iProvider
{

    public function getPackages(SymfonyStyle $io, array $config) : array;
}