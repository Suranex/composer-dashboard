<?php

namespace App\Commands\Provider\Helper;

class BuildDependenciesFromComposerData
{

    /**
     * @param mixed $lock
     * @param mixed $json
     * @return array
     */
    public function build(array $json, array $lock): array
    {
        $lockPackages = array_column(array_map([$this, 'reduceLockPackages'], $lock['packages'] ?? []), 1, 0);
        $lockPackagesDev = array_column(array_map([$this, 'reduceLockPackages'], $lock['packages-dev'] ?? []), 1, 0);
        $dependencies = [];
        foreach ($json['require'] ?? [] as $key => $constraint) {
            $dependencies[$key]['require'] = ['constraint' => $constraint, 'installed' => $lockPackages[$key] ?? ''];
        }
        foreach ($json['require-dev'] ?? [] as $key => $constraint) {
            $dependencies[$key]['dev'] = ['constraint' => $constraint, 'installed' => $lockPackagesDev[$key] ?? ''];
        }
        return $dependencies;
    }

    private function reduceLockPackages(array $package)
    {
        return [$package['name'], $package['version']];
    }
}