<?php

namespace App\Commands\Provider;

use App\Commands\Provider\Helper\BuildDependenciesFromComposerData;
use Symfony\Component\Console\Style\SymfonyStyle;

class Gitlab implements iProvider
{

    public function __construct(
        private readonly BuildDependenciesFromComposerData $buildDependenciesFromComposerData
    )
    {
    }

    public function getPackages(SymfonyStyle $io, array $config): array
    {
        $token = $config['token'];
        $serverUrl = $config['server'];
        $limit = $config['limit'];

        if (empty($token)) {
            $io->info('No Gitlab token found. Skipping');
            return [];
        }
        if (empty($limit)) $limit = 999;

        $client = new \Gitlab\Client();
        if (!empty($serverUrl)) {
            $io->info(sprintf('Searching in the %s instance.', $serverUrl));
            $client->setUrl($serverUrl);
        } else {
            $io->info('Searching in the default gitlab.com instance.');
        }
        $client->authenticate($token, \Gitlab\Client::AUTH_HTTP_TOKEN);
        $pager = new \Gitlab\ResultPager($client);

        $result = [];
        $projectGenerator = $pager->fetchAllLazy($client->projects(), 'all', [['topic' => 'symfony']]);

        foreach ($projectGenerator as $project) {
            $projectId = $project['id'];
            $projectName = $project['name'];
            $projectWebUrl = $project['web_url'];
            $defaultBranch = $project['default_branch'] ?? '';
            if (empty($defaultBranch)) {
                $io->warning(sprintf('Project %s has somehow no default branch. Skipping.', $projectName));
                continue;
            }

            try {
                $treePageGenerator = $pager->fetchAllLazy($client->repositories(), 'tree', [$projectId, ['recursive' => true]]);
                $composerJsons = [];
                foreach ($treePageGenerator as $file) {
                    if ($file['name'] === 'composer.json') {
                        $composerJsons[dirname($file['path'])]['json'] = $file;
                    } elseif ($file['name'] === 'composer.lock') {
                        $composerJsons[dirname($file['path'])]['lock'] = $file;
                    }
                }
            } catch (\Exception $exception) {
                $io->warning(sprintf('could not load tree. Exception: %s', $exception->getMessage()));
                continue;
            }

            foreach ($composerJsons as $composerJsonFiles) {
                $io->writeln(sprintf('Found composer in Repo "%s" in directory /%s', $projectWebUrl, $composerJsonFiles['json']['path']));
                $composerJson = $client->repositoryFiles()->getRawFile($projectId, $composerJsonFiles['json']['path'], $defaultBranch);
                $composerLock = ($composerJsonFiles['lock'] ?? false) ? $client->repositoryFiles()->getRawFile($projectId, $composerJsonFiles['lock']['path'], $defaultBranch) : '{}';
                try {
                    $json = (array)json_decode($composerJson, true, 512, JSON_THROW_ON_ERROR);
                } catch (\JsonException $e) {
                    $io->warning(sprintf('Failed to parse %s from project %s. Skipping.', $composerJsonFiles['json']['path'], $projectWebUrl));
                    continue;
                }
                try {
                    $lock = (array)json_decode($composerLock, true, 512, JSON_THROW_ON_ERROR);
                } catch (\JsonException $e) {
                    $io->warning(sprintf('Failed to parse %s fromject %s. Skipping.', $composerJsonFiles['json']['path'], $projectWebUrl));
                    continue;
                }

                $dependencies = $this->buildDependenciesFromComposerData->build($json, $lock);
                if (empty($dependencies)) continue;
                $result[$projectWebUrl . $composerJsonFiles['json']['path']] = [
                    'project' => [
                        'nameWithNamespace' => $project['name_with_namespace'],
                        'name' => $projectName,
                        'branch' => $defaultBranch,
                        'webUrl' => $projectWebUrl,
                        'path' => $composerJsonFiles['json']['path']
                    ],
                    'dependencies' => $dependencies,
                    'hasLock' => !empty($lock)
                ];
                $limit--;
                if ($limit <= 0) break 2;
            }
        }
        return $result;
    }
}