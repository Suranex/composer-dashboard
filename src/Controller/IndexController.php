<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    public function __construct(
        private readonly KernelInterface $appKernel
    )
    {
    }

    #[Route(path: '/', name: 'index')]
    public function __invoke() : Response
    {
        $data = json_decode(file_get_contents($this->appKernel->getProjectDir() . '/var/data.json'), true, 512, JSON_THROW_ON_ERROR);
        return $this->render('index.html.twig', [
            'data' => $data['data'],
            'packages' => $data['packages'],
            'lastChange' => date('Y-m-d H:i:s', filemtime($this->appKernel->getProjectDir() . '/var/data.json'))
        ]);
    }

}