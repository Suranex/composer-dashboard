<?php

declare(strict_types=1);

use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symplify\PackageBuilder\Strings\StringFormatConverter;

return static function (ContainerConfigurator $containerConfigurator): void {
    $services = $containerConfigurator->services();

    $services->defaults()
        ->public()
        ->autowire()
        ->autoconfigure();

    $services->load('App\\', __DIR__ . '/../src')
        ->exclude([
            __DIR__ . '/../src/Kernel.php',
        ]);

    $services->set(Client::class);

    $containerConfigurator->extension('framework', [
        'secret' => '%env(APP_SECRET)%',
    ]);

    $containerConfigurator->extension('twig', [
        'debug' => true,
        'strict_variables' => true,
        'globals' => [
            'map_id' => 'mapid',
        ],
    ]);

    $services->set(Client::class);
    $services->set(StringFormatConverter::class);
};